package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.api.service.IAuthService;
import ru.aushakov.tm.exception.empty.EmptyLoginException;
import ru.aushakov.tm.exception.empty.EmptyPasswordException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.exception.general.WrongCredentialsException;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    private User authenticatedUser;

    private final IUserRepository userRepository;

    public AuthService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void login(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        final User user = userRepository.findOneByLogin(login);
        if (user == null || !user.getPasswordHash().equals(HashUtil.salt(password))) {
            throw new WrongCredentialsException();
        }
        authenticatedUser = user;
    }

    @Override
    public void logout() {
        authenticatedUser = null;
    }

    @Override
    public boolean isUserAuthenticated() {
        return (authenticatedUser != null);
    }

    @Override
    public String getUserId() {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        return authenticatedUser.getId();
    }

    @Override
    public String getUserLogin() {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        return authenticatedUser.getLogin();
    }

    @Override
    public User getUser() {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        final String login = authenticatedUser.getLogin();
        authenticatedUser = userRepository.findOneByLogin(login);
        return authenticatedUser;
    }

    @Override
    public void changePassword(final String oldPassword, final String newPassword) {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        if (StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword)) {
            throw new EmptyPasswordException();
        }
        final String login = authenticatedUser.getLogin();
        final String oldPasswordHash = authenticatedUser.getPasswordHash();
        if (!oldPasswordHash.equals(HashUtil.salt(oldPassword))) throw new WrongCredentialsException();
        userRepository.setPassword(login, newPassword);
    }

}
