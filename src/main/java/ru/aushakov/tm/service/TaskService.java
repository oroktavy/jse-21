package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.model.Task;

import java.util.List;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository, Class currentClass) {
        this.taskRepository = taskRepository;
        this.businessRepository = taskRepository;
        this.repository = taskRepository;
        this.currentClass = currentClass;
    }

    @Override
    public Task assignTaskToProject(final String taskId, final String projectId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(taskId)) throw new EmptyIdException("task");
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        return taskRepository.assignTaskToProject(taskId, projectId, userId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(taskId)) throw new EmptyIdException("task");
        return taskRepository.unbindTaskFromProject(taskId, userId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        return taskRepository.findAllTasksByProjectId(projectId, userId);
    }

}
