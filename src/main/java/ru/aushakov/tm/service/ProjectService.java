package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.model.Project;

import java.util.Optional;

public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository,
            final Class currentClass
    ) {
        this.repository = projectRepository;
        this.businessRepository = projectRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.currentClass = currentClass;
    }

    @Override
    public Project deepDeleteProjectById(final String projectId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        final Project deletedProject = Optional
                .ofNullable(projectRepository.removeOneById(projectId, userId))
                .orElseThrow(ProjectNotFoundException::new);
        taskRepository.removeAllTasksByProjectId(projectId, userId);
        return deletedProject;
    }

}
