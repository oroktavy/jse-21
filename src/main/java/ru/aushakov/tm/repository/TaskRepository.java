package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Task assignTaskToProject(final String taskId, final String projectId, final String userId) {
        final Task task = findOneById(taskId, userId);
        Optional.ofNullable(task).ifPresent(t -> t.setProjectId(projectId));
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String taskId, final String userId) {
        final Task task = findOneById(taskId, userId);
        Optional.ofNullable(task).ifPresent(t -> t.setProjectId(null));
        return task;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId, final String userId) {
        return list.stream()
                .filter(t -> projectId.equals(t.getProjectId()) && userId.equals(t.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> removeAllTasksByProjectId(final String projectId, final String userId) {
        final List<Task> deletedTasks = list.stream()
                .filter(t -> projectId.equals(t.getProjectId()) && userId.equals(t.getUserId()))
                .collect(Collectors.toList());
        list.removeAll(deletedTasks);
        return deletedTasks;
    }

}
