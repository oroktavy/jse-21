package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        return list.stream()
                .filter(u -> login.equalsIgnoreCase(u.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User removeOneByLogin(final String login) {
        final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(list::remove);
        return user;
    }

    @Override
    public User setPassword(
            final String login,
            final String newPassword
    ) {
        final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(u -> u.setPasswordHash(HashUtil.salt(newPassword)));
        return user;
    }

    @Override
    public User updateOneById(
            final String id,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        final User user = findOneById(id);
        Optional.ofNullable(user).ifPresent(u -> {
            u.setLastName(lastName);
            u.setFirstName(firstName);
            u.setMiddleName(middleName);
        });
        return user;
    }

    @Override
    public User updateOneByLogin(
            final String login,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(u -> {
            u.setLastName(lastName);
            u.setFirstName(firstName);
            u.setMiddleName(middleName);
        });
        return user;
    }

}
