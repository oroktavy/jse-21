package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    public Collection<AbstractCommand> getTerminalCommands() {
        return commands.values();
    }

    @Override
    public void add(final AbstractCommand command) {
        commands.put(command.getName(), command);
        Optional.ofNullable(command.getArgument()).ifPresent(arg -> arguments.put(arg, command));
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return arguments.get(arg);
    }

}
