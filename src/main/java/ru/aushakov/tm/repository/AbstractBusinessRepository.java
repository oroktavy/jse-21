package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IBusinessRepository;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository() {
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream().filter(e -> e.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(
            final String userId,
            final Comparator<E> comparator
    ) {
        return list.stream().filter(e -> e.getUserId().equals(userId))
                .sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void clear(final String userId) {
        list.removeAll(list.stream().filter(e -> e.getUserId().equals(userId)).collect(Collectors.toList()));
    }

    @Override
    public E findOneById(final String id, final String userId) {
        return list.stream()
                .filter(e -> (id.equals(e.getId()) && userId.equals(e.getUserId())))
                .findFirst().orElse(null);
    }

    @Override
    public E findOneByIndex(final Integer index, final String userId) {
        final E entity = list.get(index);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    public E findOneByName(final String name, final String userId) {
        return list.stream()
                .filter(e -> (name.equals(e.getName()) && userId.equals(e.getUserId())))
                .findFirst().orElse(null);
    }

    @Override
    public E removeOneById(final String id, final String userId) {
        final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    public E removeOneByIndex(final Integer index, final String userId) {
        final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    public E removeOneByName(final String name, final String userId) {
        final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    public E updateOneById(
            final String id,
            final String userId,
            final String name,
            final String description
    ) {
        final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return entity;
    }

    @Override
    public E updateOneByIndex(
            final Integer index,
            final String userId,
            final String name,
            final String description
    ) {
        final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return entity;
    }

    @Override
    public E startOneById(final String id, final String userId) {
        final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return entity;
    }

    @Override
    public E startOneByIndex(final Integer index, final String userId) {
        final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return entity;
    }

    @Override
    public E startOneByName(final String name, final String userId) {
        final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return entity;
    }

    @Override
    public E finishOneById(final String id, final String userId) {
        final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.COMPLETED));
        return entity;
    }

    @Override
    public E finishOneByIndex(final Integer index, final String userId) {
        final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.COMPLETED));
        return entity;
    }

    @Override
    public E finishOneByName(final String name, final String userId) {
        final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.COMPLETED));
        return entity;
    }

    @Override
    public E changeOneStatusById(final String id, final Status status, final String userId) {
        final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(status));
        return entity;
    }

    @Override
    public E changeOneStatusByIndex(final Integer index, final Status status, final String userId) {
        final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(status));
        return entity;
    }

    @Override
    public E changeOneStatusByName(final String name, final Status status, final String userId) {
        final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(status));
        return entity;
    }

}
