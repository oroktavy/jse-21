package ru.aushakov.tm.command.system;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

import java.util.Collection;

public class ArgumentsCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_ARGUMENTS;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show application arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENT LIST]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (StringUtils.isEmpty(argument)) continue;
            System.out.println(argument);
        }
    }

}
