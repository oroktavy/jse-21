package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;

public class VersionCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_VERSION;
    }

    @Override
    public String getArgument() {
        return ArgumentConst.ARG_VERSION;
    }

    @Override
    public String getDescription() {
        return "Show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
