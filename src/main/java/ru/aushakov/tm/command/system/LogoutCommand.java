package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_LOGOUT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
