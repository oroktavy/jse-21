package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;

public class AboutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_ABOUT;
    }

    @Override
    public String getArgument() {
        return ArgumentConst.ARG_ABOUT;
    }

    @Override
    public String getDescription() {
        return "Show general application info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: ANDREY USHAKOV");
        System.out.println("E-MAIL: oroktavy@gmail.com");
    }

}
