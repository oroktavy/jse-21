package ru.aushakov.tm.command.system;

import ru.aushakov.tm.api.service.IAuthService;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.util.TerminalUtil;

public class ChangePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_CHANGE_PASSWORD;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change your current password";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String oldPassword = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        authService.changePassword(oldPassword, newPassword);
        authService.logout();
    }

}
