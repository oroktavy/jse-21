package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_START_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService().startOneByName(name, userId))
                .orElseThrow(TaskNotFoundException::new);
    }

}
