package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change task status by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        final String statusId = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService().changeOneStatusByName(name, statusId, userId))
                .orElseThrow(TaskNotFoundException::new);
    }

}
