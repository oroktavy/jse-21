package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskAssignToProjectCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_ASSIGN_TO_PROJECT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Assign task to project";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ASSIGN TASK TO PROJECT]");
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID TO ASSIGN TO:");
        final String projectId = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService().assignTaskToProject(taskId, projectId, userId))
                .orElseThrow(TaskNotFoundException::new);
    }

}
