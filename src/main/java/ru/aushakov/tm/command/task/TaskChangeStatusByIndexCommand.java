package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change task status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        final String statusId = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService().changeOneStatusByIndex(index, statusId, userId))
                .orElseThrow(TaskNotFoundException::new);
    }

}
