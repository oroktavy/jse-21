package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_UNBIND_FROM_PROJECT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Unbind task from project";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK FROM ITS PROJECT]");
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService().unbindTaskFromProject(taskId, userId))
                .orElseThrow(TaskNotFoundException::new);
    }

}
