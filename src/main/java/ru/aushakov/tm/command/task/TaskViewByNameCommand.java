package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskViewByNameCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_VIEW_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "View task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = Optional
                .ofNullable(serviceLocator.getTaskService().findOneByName(name, userId))
                .orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
