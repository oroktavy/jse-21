package ru.aushakov.tm.command.user;

import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class UserSetPasswordCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return TerminalConst.USER_SET_PASSWORD;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Set user password";
    }

    @Override
    public void execute() {
        System.out.println("[SET USER PASSWORD]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getUserService().setPassword(login, newPassword))
                .orElseThrow(UserNotFoundException::new);
    }

}
