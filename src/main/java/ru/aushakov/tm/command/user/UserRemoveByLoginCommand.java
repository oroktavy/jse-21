package ru.aushakov.tm.command.user;

import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return TerminalConst.USER_REMOVE_BY_LOGIN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getUserService().removeOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
    }

}
