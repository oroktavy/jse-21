package ru.aushakov.tm.command.project;

import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_START_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Optional.ofNullable(serviceLocator.getProjectService().startOneByIndex(index, userId))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
