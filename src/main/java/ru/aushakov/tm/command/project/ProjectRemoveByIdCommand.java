package ru.aushakov.tm.command.project;

import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getProjectService().removeOneById(id, userId))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
