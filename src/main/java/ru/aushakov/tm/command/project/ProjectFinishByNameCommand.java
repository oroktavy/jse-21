package ru.aushakov.tm.command.project;

import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_FINISH_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getProjectService().finishOneByName(name, userId))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
