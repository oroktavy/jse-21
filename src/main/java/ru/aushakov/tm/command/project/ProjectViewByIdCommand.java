package ru.aushakov.tm.command.project;

import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectViewByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_VIEW_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "View project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = Optional
                .ofNullable(serviceLocator.getProjectService().findOneById(id, userId))
                .orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

}
