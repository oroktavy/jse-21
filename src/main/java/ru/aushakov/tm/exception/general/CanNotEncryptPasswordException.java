package ru.aushakov.tm.exception.general;

public class CanNotEncryptPasswordException extends RuntimeException {

    public CanNotEncryptPasswordException(Throwable cause) {
        super("Can not encrypt password using MD5!", cause);
    }

}
