package ru.aushakov.tm.exception.general;

public class InvalidRoleException extends RuntimeException {

    public InvalidRoleException() {
        super("Role is invalid!");
    }

    public InvalidRoleException(final String roleId) {
        super("The value '" + roleId + "' entered is not a valid role!");
    }

}
