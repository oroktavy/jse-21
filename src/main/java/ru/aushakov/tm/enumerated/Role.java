package ru.aushakov.tm.enumerated;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    private static final Role[] staticValues = values();

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static Role toRole(final String roleId) {
        for (final Role role : staticValues) {
            if (role.name().equalsIgnoreCase(roleId)) return role;
        }
        return null;
    }

}
